const Koa = require("koa");
const cors = require("@koa/cors");
// const Router = require("@koa/router");

const app = new Koa();
app.use(cors());

function registrator(cRouter) {
  app.use(cRouter.routes());
}

const SantriHandler = require("./santri_handler.js")
const PostHandler = require("./handlers/api_v1/post_handler.js");
const UserPostHandler = require("./handlers/api_v1/user_post_handler.js");
const AccountHandler = require("./handlers/api_v1/account_handler.js");

SantriHandler.registerHandlerApiV1(registrator);
PostHandler.registerHandlerApiV1(registrator);
UserPostHandler.registerHandlerApiV1(registrator);
AccountHandler.registerHandlerApiV1(registrator);

app.listen(3030);
