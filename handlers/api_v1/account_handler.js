const Router = require("@koa/router");
const bodyParser = require("koa-bodyparser");
// const {parse_json} = require("./json_utils.js");
const JWT = require("jsonwebtoken");

const users = [
  { id: 1,
    name: "John",
    username: "frontend_dev",
    password: "react"
  },
  { id: 2,
    name: "Doe",
    username: "frontend_dev_2",
    password: "vue"
  }
];

const json_parser = bodyParser();

exports.registerHandlerApiV1 = function(register) {
  const router = new Router();
  router.prefix("/api/v1/account");
  router.use(bodyParser());
  router.post("/token", signinAccount);
  router.post("/register", registerAccount);
  // router.post("/account/")
  register(router);
}

async function signinAccount(ctx) {
  // console.log("here")
  // const body = await parse_json(ctx.req);
  const body = ctx.request.body;
  console.log(body);
  try {
    const result = validateSigninData(body);
    if (result.isError) {
      console.log(result);
      ctx.status = 403;
      ctx.body = JSON.stringify(result.errors);
    } else {
      console.log("here")
      const data = result.data;
      const user = verify_user(data.username, data.password)
      if (user == null) {
        console.log("here");
        ctx.status = 403;
        ctx.body = "error"
      } else {
        console.log(JWT);
        ctx.status = 200;
        ctx.body = JSON.stringify({
          token: JWT.sign({id: user.id, name: user.name}, "Kucing Terbang")
        })
        console.log("ok")
      }
    }
    // return ctx;
  } catch(e) {
    console.log(e);
  }


}

function verify_user(username, password) {
  return users.find((el) => el.username == username && el.password == password)
}

function registerAccount(ctx) {

}


function validateSigninData(body) {
  const error = [];
  if (body.username === undefined) {
    error.push({
      field: "username",
      message: ["Username field must exist"]
    });
  } else {
    let msg = [];
    if (typeof(body.username) != "string") {
      msg.push("Username must a String!");
    } else if (body.username == null || body.username == "") {
      msg.push("Username is required! must not a empty string");
    } else if (typeof(body.username) == "string" && body.length <= 3) {
      msg.push("Username character must greater than 3");
    }
    if (msg.length > 0) {
      error.push({
        field: "username",
        message: msg
      });
    }
  }

  if (body.password === undefined) {
    error.push({
      field: "password",
      message: ["Password field must exist"]
    });
  } else {
    let msg = [];
    if (typeof(body.password) != "string") {
      msg.push("Password must a String!");
    } else if (body.password == null || body.password == "") {
      msg.push("Password is required! must not a empty string")
    }

    if (msg.length > 0) {
      error.push({
        field: "password",
        message: msg
      })
    }
  }

  return {
    isError: error.length != 0,
    errors: evaluate_error(error),
    data: {
      username: body.username,
      password: body.password
    }
  };
}

function evaluate_error(error) {
  if (error.length == 0) {
    return null;
  } else {
    var err = {};
    error.forEach((el) => {
      err[el.field] = el.message
    });
    return err;
  }
}
