const Router = require("@koa/router");
const {jwt_middleware} = require("./jwt_utils.js");
const Post = require("../../post/data.js");

exports.registerHandlerApiV1 = function(register) {
  const router = new Router();
  router.prefix("/api/v1/public/post");
  // router.use(jwt)
  router.get("/", listPost);
  router.get("/:id", getPost);
  register(router);
}

function listPost(ctx) {
  ctx.body = JSON.stringify({
    data: Post.list_posts()
  });
}

function getPost(ctx) {
  const post = Post.get_post(Number(ctx.params.id))
  if (post == null) {
    ctx.status = 404;
    ctx.body = "Post not found";
  } else {
    ctx.body = JSON.stringify({
      data: post
    })
  }

}
