const bodyParser = require("koa-bodyparser");
const Router = require("@koa/router");
const Post = require("../../post/data.js");
const {jwt_middleware} = require("./jwt_utils.js");
const {parse_json} = require("./json_utils.js");

exports.registerHandlerApiV1 = function(register) {

  const router = new Router();
  router.prefix("/api/v1/user/post");
  router.use(jwt_middleware);
  // const jsonParser = bodyParser();
  router.use(bodyParser());
  router.use("/:id", check_user_post);



  router.get("/", index);
  router.get("/:id", getPost);
  router.post("/",  add);
  router.delete("/:id", delPost);
  router.put("/:id", edit);

  register(router);
}

async function check_user_post(ctx,next) {
  // console.log()
  const post = Post.get_author_post(ctx.params.id, ctx.params.user.id);
  if (post == null) {
    ctx.status = 403;
    ctx.body = JSON.stringify({message: "User not the owner of post"});
  } else {
    ctx.params["post"] = post;
    await next(ctx);
  }
}

function getPost(ctx) {
  const post = ctx.params.post;
  if (post == null) {
    ctx.status = 404;
    ctx.body = "post not found";
  } else {
    ctx.body = JSON.stringify({
      data: post
    });
  }
}

function index(ctx) {
  const posts = Post.list_posts().filter(
    (el) => el.author_id == ctx.params.user.id
  );
  ctx.body = JSON.stringify({
    data: posts
  });
}

function add(ctx,next) {
  try {
    // console.log("hello Post")
    // const body = await parse_json(ctx.req);
    const body = ctx.request.body
    const user = ctx.params.user;
    if (body.title == "" || body.title == null || body.content == "" || body.content == null) {
      ctx.status = 422;
      ctx.body = "{\"message\": \"invalid data structure, rechek it!\"}"
      return ctx;
    }
    // console.log(ctx)
    const new_post = Post.add_post(
      body.title,
      body.content,
      user.id
    )
    ctx.status = 200;
    ctx.body = JSON.stringify({data: new_post});
  } catch (e) {
    console.log(e)
  }
  // console.log(ctx);
  // next();s
}

function get(ctx) {
  ctx.body = JSON.stringify({
    data: ctx.params.post
  })
}

function edit(ctx) {
  const data = ctx.request.body;
  const post = Post.get_author_post(Number(ctx.params.id),ctx.params.user.id);
  const updated_post = {
    ...post,
    title: data.title,
    content: data.content
  };

  if (Post.update_post(post.id, updated_post)) {
    ctx.body = JSON.stringify({
      data: updated_post
    });
  } else {
    ctx.status = 500;
  }

}

function delPost(ctx) {
  if (Post.delete_post(ctx.params.post.id)) {
    ctx.status = 204;
  } else {
    ctx.status = 500;
  }
}
