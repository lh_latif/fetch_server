exports.parse_json = function(stream) {
  return new Promise((resolve,reject) => {
    let data = "";
    stream.on("readable",() => {
      const d = stream.read();
      // console.log(data);
      if (d != null) {
        data += d;
      }
    });

    stream.on("end",() => {
      try {
        resolve(JSON.parse(data));
      } catch (e) {
        reject(e);
      }

    });

    stream.on("error",(err) => {
      reject(err)
    })

    stream.resume();
  });
}
