const JWT = require("jsonwebtoken");

exports.jwt_middleware = async function(ctx,next) {
  // console.log(ctx.headers)
  const auth = ctx.headers.authorization;
  if (auth == null) {
    return unauthorize(ctx);
  }
  const token = auth.slice(7);
  if (token == null || token == "") {
    return unauthorize(ctx);
  }
  // console.log(token);
  const payload = JWT.verify(token, "Kucing Terbang");
  // console.log(payload);
  ctx.params["user"] = {
    id: payload.id,
    name: payload.name
  };
  await next();
}

function unauthorize(ctx) {
  ctx.status = 403;
  ctx.body = "Not Authorized"
}
