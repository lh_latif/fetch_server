let last_id = 1;
const posts = [
  { id: 1,
    title: "Data Post Dari Server",
    content: `Node js adalah platform untuk menjalankan node pada desktop`,
    author_id: 1
  }
];


exports.get_post = function(id) {
  return posts.find((el) => el.id === id);
}

exports.get_author_post = function(id,author_id) {
  return posts.find((el) => el.id == id && el.author_id == author_id);
}

exports.add_post = function(title, content, author_id) {
  last_id++;
  const new_post = {
    id: last_id,
    title: title,
    content: content,
    author_id: author_id
  }
  posts.push(new_post);
  return new_post;
}

exports.list_posts = function() {
  return posts;
}

exports.update_post = function(id,post) {
  const index = posts.findIndex((el) => el.id === id)
  if (index < 0) {
    return false;
  } else {
    posts[index] = post;
    return true;
  }
}

exports.delete_post = function(id) {
  const index = posts.findIndex((el) => el.id === id);

  if (index < 0) {
    return false;
  } else {
    posts.splice(index, 1);
    return true;
  }
}
