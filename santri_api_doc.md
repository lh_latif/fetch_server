Santri API

# GET | /api/v1/santri

Menampilkan list santri. Data dalam bentuk stringified. Contoh hasil response:
```
{
  data: [
    { id: 1,
      name: "Someone",
      division: "Frontend",
      origin: "Kota Yogyakarta"
    },
    { id: 2,
      name: "Santri",
      division: "Backend",
      origin: "Bantul"
    }
  ]
}
```
response:
### 200
Berhasil



# GET | /api/v1/santri/[id]

Mendapatkan data sebuah santri

### 200
Berhasil


# POST | /api/v1/santri

Membuat data santri baru.
Data yang dikirimkan merupakan FormData dengan struktur sebagai berikut:

| field | type | desc |
|-------|------|------|
| name | String |  |
| division | String |  |
| origin | String | Asal Kedatangan |

response:
### 200

  Berhasil

### 422

  struktur FormData tidak lengkap


# PUT | /api/v1/santri/[id]

Mengedit data santri.
Struktur FormData sama dengan `POST | /api/v1/santri`

response:
### 200
  Berhasil

### 422
  Struktur FormData tidak lengkap

### 403
  Santri dengan id yang diinput tidak ada


# DELETE | /api/v1/santri/[id]
Menghapus data seorang santri
