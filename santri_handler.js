const multer = require("@koa/multer");
const upload = multer();
const mpParser = upload.none();
// console.log(multer, upload);
const Router = require("@koa/router");

// function useMpParser(handler) {
//   mpPrser()
// }
// apiv1
exports.registerHandlerApiV1 = function(register) {
  const router = new Router();
  router.prefix("/api/v1");

  router.get("/santri",listSantri);
  router.use("/santri",mpParser);
  router.post("/santri", addSantri);
  router.put("/santri/:id", editSantri);
  router.delete("/santri/:id", deleteSantri);

  register(router);
}

var last_id = 0;
var santri = [];

function listSantri(ctx) {
  ctx.body = JSON.stringify({
    data: santri
  });
}

async function addSantri(ctx,next) {
  const data = ctx.request.body;
  if (data.name == null
    || data.name == ""
    || data.division == ""
    || data.division == null
    || data.origin == ""
    || data.origin == null
  ) {
    ctx.status = 422;
    ctx.body = "error";
    return ctx;
  }



  last_id++;
  santri.push({
    id: last_id,
    name: data.name,
    division: data.division,
    origin: data.origin
  });
  resp = ctx.response;
  resp.status = 200;
  resp.message = "";

  // ctx.status = 500;
}

function editSantri(ctx) {
  const id = Number(ctx.params.id);
  const data = ctx.request.body;
  if (data.id == null
    || data.name == null
    || data.name == ""
    || data.division == null
    || data.divisino == ""
    || data.origin == null
    || data.origin == ""
  ) {
    ctx.status = 422;
    ctx.body = "Error";
  }
  let stri;
  santri.map((el) => {
    if (el.id == id) {
      el.name = data.name;
      el.division = data.division;
      el.origin = data.origin;
      stri = el;
    }

    return el
  });

  if (stri == null) {
    ctx.status = 404;
    ctx.body = "not_found";
  } else {
    ctx.status = 200;
    ctx.body = JSON.stringify({
      data: stri
    })
  }

}

function deleteSantri(ctx) {
  let stri;
  let isExist;
  const id = ctx.params.id;
  santri = santri.filter((el) => {
    if (el.id == id) {
      stri = el;
      isExist = true;
      return false;
    } else {
      return true;
    }
  });

  if (isExist != null) {
    ctx.status = 204;
    ctx.body = ""
  } else {
    ctx.status = 403;
    ctx.body = "not_found"
  }
}


function read_body(stream) {
  return new Promise((resolve,reject) => {
    let data = "";
    stream.on("readable",() => {
      data += stream.read();
    });

    stream.on("end",() => {
      resolve(data);
    });

    stream.on("error",(err) => {
      reject(err)
    })

    stream.resume();
  });
}
