Endpoint ini merupakan endpoint untuk mendapatkan semua data "Post" yang tersimpan diserver. Endpoint tidak dijaga oleh JWT jadi tidak perlu mengikut sertakan token JWT.

# GET | /api/v1/public/post
Mendapatkan list post dari semua user.
contoh response:
```javascript
{
  data: [
    {
      id: 1,
      title: "Ini post",
      content: "Ini Content",
      author_id: 2
    },
    {
      id: 2,
      title: "Ini post 2",
      content: "Ini Content 2",
      author_id: 1
    }
  ]
}
```

# GET | /api/v1/public/post/{id}
Mendapatkan sebuah post berdasarkan id yang dipilih milik user siapapun.
contoh response:
```javascript
{
  data: {
    id: 1,
    title: "Ini title",
    content: "Ini content",
    author_id: 1
  }
}
```
