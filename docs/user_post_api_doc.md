endpoint ini berguna untuk memanipulasi data "Post" yag dimiliki user.
Manipulasi data yang di maksud adalah menambah post, list post milik user, edit post milik user, dan menghapus post milik user.


====================================
# SEMUA ENDPOINT MENGGUNAKAN JWT/Token
====================================

# GET | /api/v1/user/post
Menampilkan list "Post" milik user.
contoh response:
```javascript
{
  data: [
    {
      id: 1,
      title: "Ini post",
      content: "Ini Content",
      author_id: 1
    },
    {
      id: 2,
      title: "Ini post 2",
      content: "Ini Content 2",
      author_id: 1
    }
  ]
}
```

# POST | /api/v1/user/post
Untuk menambah post milik user ke server.
data yang harus dikirim
```js
{
  title: "Ini Title",
  content: "Ini Content"
}
```

# GET | /api/v1/user/post/{id}
Untuk mendapatkan post milik user sesuai id yang dipilih.
tidak perlu mengirimkan data.
contoh response:
```javascript
{
  data: {
    id: 1,
    title: "Ini title",
    content: "Ini content",
    author_id: 1
  }
}
```

# PUT | /api/v1/user/post/{id}
Untuk mengubah data post sesuai id yang dipilih.
data yang harus dikirim.
```js
{
  title: "Ini Title",
  content: "Ini Content"
}
```

Dapat error jika post yang dipilih tidak dimiliki oleh user.


# DELETE | /api/v1/user/post/{id}
Untuk menghapus data post user sesuai id yang dipilih.
tidak perlu mengirimkan data.
response status berhasil adalah '204'.
